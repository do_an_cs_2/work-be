package com.tuandev.workbe.repositories;

import com.tuandev.workbe.entities.Employer;
import com.tuandev.workbe.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findByEmployer(Employer employer);
}
