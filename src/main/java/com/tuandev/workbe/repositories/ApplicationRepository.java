package com.tuandev.workbe.repositories;

import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {
    List<Application> findByUser(User user);

    List<Application> findByJob(Job job);
}
