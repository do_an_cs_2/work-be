package com.tuandev.workbe.repositories;

import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Field;
import com.tuandev.workbe.entities.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {
    List<Job> findByCompany(Company company);

    List<Job> findByNameContains(String searchTerm);

    List<Job> findByField(Field field);
}
