package com.tuandev.workbe.repositories;

import com.tuandev.workbe.entities.FavoriteJob;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteJobRepository extends JpaRepository<FavoriteJob, Integer> {
    List<FavoriteJob> findFavoriteJobByUser(User user);
    FavoriteJob findByUserAndJob(User user, Job job);
}
