package com.tuandev.workbe.entities;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "companies")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "company_id")
    private Integer companyId;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "website")
    private String website;

    @Column(name = "description")
    private String description;

    @Column(name = "location")
    private String location;

    @Column(name = "logo")
    private String logo;

    @Column(name = "banner")
    private String banner;
}
