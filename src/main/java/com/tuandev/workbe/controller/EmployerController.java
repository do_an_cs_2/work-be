package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.AuthDto;
import com.tuandev.workbe.dto.EmployerDto;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Employer;
import com.tuandev.workbe.services.EmployerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/employers")
@RequiredArgsConstructor
public class EmployerController {
    private final EmployerService employerService;
    private final PasswordEncoder passwordEncoder;

    @GetMapping
    public List<Employer> getAllEmployers() {
        return employerService.getAllEmployers();
    }

    @PostMapping
    public ResponseEntity<Object> createEmployer(@Valid @RequestBody EmployerDto employerDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(employerService.createEmployer(employerDto));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody AuthDto authDto) {
        Optional<Employer> employerOptional = employerService.getEmployerByEmail(authDto.getEmail());
        if (employerOptional.isEmpty() || employerOptional.get().getPassword().equals(passwordEncoder.encode(authDto.getPassword()))) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid email or password");
        }
        return ResponseEntity.ok(employerOptional.get());
    }

}
