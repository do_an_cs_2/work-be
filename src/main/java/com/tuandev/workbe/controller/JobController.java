package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.JobDto;
import com.tuandev.workbe.dto.ResponseMessage;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.services.JobService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobController {
    private final JobService jobService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Job> getAllJobs() {
        return jobService.getAllJobs();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getJobById(@PathVariable Integer id) {
        return jobService.getJobById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<Job> getJobByCompany(@RequestParam("companyId") Integer companyId) {
        return jobService.getJobByCompany(companyId);
    }

    @GetMapping("/search")
    public List<Job> getJobByName(@RequestParam("name") String name) {
        return jobService.getJobByName(name);
    }

    @GetMapping("/field")
    public List<Job> getJobByName(@RequestParam("id") Integer id) {
        return jobService.getJobByField(id);
    }

    @PostMapping
    public ResponseEntity<Object> createJob(@Valid @RequestBody JobDto jobDto) {
        System.out.println(jobDto.toString());
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(jobService.createJob(jobDto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteJob(@PathVariable("id") Integer id) {
        Optional<Job> jobOptional =  jobService.getJobById(id);
        if (jobOptional.isPresent()) {
            jobService.deleteJobById(id);
            return ResponseEntity.ok(new ResponseMessage("Job with ID " + id + " deleted successfully."));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("Job with ID " + id + " not found."));
    }
}
