package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.CompanyDto;
import com.tuandev.workbe.dto.ResponseMessage;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.services.CloudinaryService;
import com.tuandev.workbe.services.CompanyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/companies")
public class CompanyController {
    private final CompanyService companyService;
    private final CloudinaryService cloudinaryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Company> getAllCompanies() {
        return companyService.getAllCompanies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCompanyById(@PathVariable("id") Integer id) {
        return companyService.getCompanyById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Object> createCompany(@Valid @ModelAttribute CompanyDto companyDto) {
        try {
            String logoUrl = cloudinaryService.uploadFile(companyDto.getLogo(), "/logo");
            Company company = Company.builder()
                    .name(companyDto.getName())
                    .website(companyDto.getWebsite())
                    .location(companyDto.getLocation())
                    .logo(logoUrl)
                    .build();
            return ResponseEntity.status(HttpStatus.CREATED).body(companyService.createCompany(company));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCompany(@PathVariable("id") Integer id, @ModelAttribute CompanyDto companyDto) {
        Optional<Company> companyOptional = companyService.getCompanyById(id);
        if (companyOptional.isPresent()) {
            Company company = companyOptional.get();

            String bannerUrl = null;
            if (companyDto.getBanner() != null) {
                try {
                    bannerUrl = cloudinaryService.uploadFile(companyDto.getBanner(), "/banner");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            company.setName(companyDto.getName());
            company.setLocation(companyDto.getLocation());
            company.setWebsite(companyDto.getWebsite());
            company.setDescription(companyDto.getDescription());
            company.setBanner(bannerUrl);
            return ResponseEntity.ok(company);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company with ID " + id + " not found.");
    };

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCompany(@PathVariable("id") Integer id) {
        Optional<Company> companyOptional =  companyService.getCompanyById(id);
        if (companyOptional.isPresent()) {
            companyService.deleteCompanyById(id);
            return ResponseEntity.ok(new ResponseMessage("Company with ID " + id + " deleted successfully."));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("Company with ID " + id + " not found."));
    }
}
