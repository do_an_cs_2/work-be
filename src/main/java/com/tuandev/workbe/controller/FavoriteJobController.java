package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.FavoriteJobRequest;
import com.tuandev.workbe.dto.FavoriteJobResponse;
import com.tuandev.workbe.services.FavoriteJobService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/favorite-job")
@RequiredArgsConstructor
public class FavoriteJobController {
    private final FavoriteJobService favoriteJobService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<FavoriteJobResponse> getAllFavoriteJob() {
        return favoriteJobService.getAllFavoriteJob();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public FavoriteJobResponse getFavoriteJobById(@PathVariable("id") Integer id) {
        return favoriteJobService.getFavoriteById(id);
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<FavoriteJobResponse> getFavoriteJobByUser(@PathVariable("id") Integer id) {
        return favoriteJobService.getFavoriteJobByUser(id);
    }

    @GetMapping("/check/{userId}/{jobId}")
    @ResponseStatus(HttpStatus.OK)
    public FavoriteJobResponse isFavoriteJob(@PathVariable Integer userId, @PathVariable Integer jobId) {
        return favoriteJobService.getFavoriteJob(userId, jobId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public FavoriteJobResponse createFavoriteJob(@RequestBody FavoriteJobRequest favoriteJobRequest) {
        return favoriteJobService.createFavoriteJob(favoriteJobRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFavoriteJob(@PathVariable("id") Integer id) {
        favoriteJobService.deleteFavoriteJobById(id);
    }

}
