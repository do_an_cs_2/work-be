package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.ResponseMessage;
import com.tuandev.workbe.dto.UserDto;
import com.tuandev.workbe.entities.User;
import com.tuandev.workbe.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Integer id) {
        Optional<User> userOptional = userService.getUserById(id);
        return userOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/email")
    public ResponseEntity<User> getUserByEmail(@RequestParam String email) {
        Optional<User> userOptional = userService.getUserByEmail(email);
        return userOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Object> createUser(@Valid @RequestBody UserDto userDto) {
        Optional<User> user = userService.getUserByEmail(userDto.getEmail());
        if (user.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage("Email already exists."));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.creeateUser(userDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Integer id, @RequestBody UserDto userDto) {
        Optional<User> userOptional = userService.getUserById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setFullName(userDto.getFullName());
            user.setAddress(userDto.getAddress());
            user.setDateOfBirth(userDto.getDateOfBirth());
            user.setPhone(userDto.getPhone());
            user.setGender(userDto.getGender());
            return ResponseEntity.ok(userService.updateUser(id, user));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("User not found."));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Integer id) {
        Optional<User> userOptional = userService.getUserById(id);
        if (userOptional.isPresent()) {
            userService.deleteUser(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("User not found."));
    }
}
