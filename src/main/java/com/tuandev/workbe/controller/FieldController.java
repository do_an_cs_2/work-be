package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.ResponseMessage;
import com.tuandev.workbe.entities.Field;
import com.tuandev.workbe.services.FieldService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/fields")
@RequiredArgsConstructor
public class FieldController {
    private final FieldService fieldService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Field> getAllFields() {
        return fieldService.getAllFields();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteField(@PathVariable("id") Integer id) {
        if (fieldService.getFieldById(id).isPresent()) {
            fieldService.deleteFieldById(id);
            return ResponseEntity.ok(new ResponseMessage("Deleted field width id " + id + " successfully."));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("Delete field width id " + id + " not found."));
    }
}
