package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.ApplicationDto;
import com.tuandev.workbe.dto.EmailApplyDto;
import com.tuandev.workbe.dto.ResponseMessage;
import com.tuandev.workbe.dto.StatusDto;
import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import com.tuandev.workbe.services.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/applications")
@RequiredArgsConstructor
public class ApplicationController {
    private final CloudinaryService cloudinaryService;
    private final ApplicationService applicationService;
    private final UserService userService;
    private final JobService jobService;
    private final EmailService emailService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<Application> getAllApplications() {
        return applicationService.getAllApplications();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getApplicationById(@PathVariable("id") Integer id) {
        return applicationService.getApplicationById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<Object> getApplicationByIdUser(@RequestParam("email") String email) {
        Optional<User> user = userService.getUserByEmail(email);

        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("User not found"));
        }

        List<Application> applications = applicationService.getApplicationsByUser(user.get());

        return ResponseEntity.status(HttpStatus.OK).body(applications);
    }

    @GetMapping("/job")
    @ResponseStatus(HttpStatus.OK)
    public List<Application> getApplicationByJob(@RequestParam("id") Integer jobId) {
        return applicationService.getApplicationByJob(jobId);
    }

    @PostMapping
    public ResponseEntity<Object> createApplication(@Valid @ModelAttribute ApplicationDto applicationDto) {
        try {
            String cvUrl = cloudinaryService.uploadFile(applicationDto.getCv(), "/cv");
            Optional<User> user = userService.getUserByEmail(applicationDto.getEmail());
            Optional<Job> job = jobService.getJobById(applicationDto.getJobId());

            if (user.isEmpty() || job.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("Job of User not found"));
            }

            Application application = Application.builder()
                    .user(user.get())
                    .job(job.get())
                    .cv(cvUrl)
                    .status(Application.Status.Pending)
                    .coverLetter(applicationDto.getCoverLetter())
                    .build();
            application = applicationService.createApplication(application);
            emailService.sendEmailApply(new EmailApplyDto(application.getUser(), application.getJob()));
            return ResponseEntity.status(HttpStatus.CREATED).body(application);
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/{id}")
    public Application updateApplication(@PathVariable("id") Integer id, @RequestBody StatusDto statusDto) {
        Application application = applicationService.updateApplicationStatus(id, statusDto.getStatus());
        emailService.sendEmailUpdateStatus(application);
        return application;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteJob(@PathVariable("id") Integer id) {
        Optional<Application> applicationOptional =  applicationService.getApplicationById(id);
        if (applicationOptional.isPresent()) {
            applicationService.deleteApplicationById(id);
            return ResponseEntity.ok(new ResponseMessage("Apply with ID " + id + " deleted successfully."));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage("Apply with ID " + id + " not found."));
    }

}
