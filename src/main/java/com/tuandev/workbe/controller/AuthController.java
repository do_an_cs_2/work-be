package com.tuandev.workbe.controller;

import com.tuandev.workbe.dto.AuthDto;
import com.tuandev.workbe.dto.ResponseJwt;
import com.tuandev.workbe.services.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping
    public ResponseJwt auth(@Valid @RequestBody AuthDto authDto) {
        return authService.login(authDto);
    }
}
