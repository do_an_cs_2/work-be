package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Employer;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostRequest {
    private Integer employerId;

    private String title;

    private String introduction;

    private String content;
}
