package com.tuandev.workbe.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDto {
    @NotNull
    @Email
    private String email;

    @NotNull
    private String password;
}
