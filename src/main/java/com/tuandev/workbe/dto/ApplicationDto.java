package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Application;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class ApplicationDto {
    @NotNull
    private String email;

    @NotNull
    private Integer jobId;

    @NotNull
    private String coverLetter;

    @NotNull
    private MultipartFile cv;

    private Application.Status status;
}
