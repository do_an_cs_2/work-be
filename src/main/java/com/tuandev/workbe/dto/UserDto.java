package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.User;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserDto {

    private String fullName;

    @Email
    @NotNull
    private String email;

    private String password;

    private String phone;

    private Date dateOfBirth;

    private User.Gender gender;

    private String address;
}
