package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FavoriteJobResponse {
    private Integer id;
    private User user;
    private Job job;
}
