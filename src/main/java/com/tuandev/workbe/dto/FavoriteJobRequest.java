package com.tuandev.workbe.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FavoriteJobRequest {
    private Integer userId;
    private Integer jobId;
}
