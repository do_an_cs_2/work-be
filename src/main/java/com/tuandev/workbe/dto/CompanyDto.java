package com.tuandev.workbe.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class CompanyDto {
    @NotNull
    private String name;

    @NotNull
    private String website;

    private String description;

    @NotNull
    private String location;

    @NotNull
    private MultipartFile logo;

    private MultipartFile banner;
}
