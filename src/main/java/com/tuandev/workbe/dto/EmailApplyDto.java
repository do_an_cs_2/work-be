package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EmailApplyDto {
    private User user;
    private Job job;
}
