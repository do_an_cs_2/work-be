package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Field;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JobDto {

    @NotNull
    private String name;

    private String description;

    private String offer;

    private String requirement;

    @NotNull
    private String salary;

    @NotNull
    private String location;

    @NotNull
    private Field field;

    @NotNull
    private Company company;
}
