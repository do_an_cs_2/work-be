package com.tuandev.workbe.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ResponseJwt {
    private String token;
    private String refreshToken;
}
