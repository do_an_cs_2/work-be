package com.tuandev.workbe.dto;

import com.tuandev.workbe.entities.Application;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@Getter
@Setter
public class StatusDto {
    private Application.Status status;
}
