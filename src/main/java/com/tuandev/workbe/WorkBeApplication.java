package com.tuandev.workbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkBeApplication.class, args);
    }

}
