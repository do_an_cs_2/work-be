package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.CompanyDto;
import com.tuandev.workbe.entities.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    List<Company> getAllCompanies();
    Optional<Company> getCompanyById(Integer id);
    Company createCompany(Company company);
    Company updateCompany(Company company);

    void deleteCompanyById(Integer id);
}
