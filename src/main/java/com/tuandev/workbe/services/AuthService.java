package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.AuthDto;
import com.tuandev.workbe.dto.ResponseJwt;

public interface AuthService {
    ResponseJwt login(AuthDto authDto);
}
