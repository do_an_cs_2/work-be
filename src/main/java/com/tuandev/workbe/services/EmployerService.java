package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.EmployerDto;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Employer;

import java.util.List;
import java.util.Optional;

public interface EmployerService {

    List<Employer> getAllEmployers();

    Employer getEmployerById(Integer id);

    Optional<Employer> getEmployerByEmail(String email);

    Employer createEmployer(EmployerDto employerDto);

    Employer updateEmployer(Integer id, EmployerDto employerDto);

    void deleteEmployer(Integer id);
}
