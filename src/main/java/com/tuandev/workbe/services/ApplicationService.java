package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.ApplicationDto;
import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.User;

import java.util.List;
import java.util.Optional;

public interface ApplicationService {
    Optional<Application> getApplicationById(Integer id);

    List<Application> getAllApplications();

    List<Application> getApplicationsByUser(User user);

    List<Application> getApplicationByJob(Integer jobId);

    Application createApplication(Application application);

    Application updateApplicationStatus(Integer id, Application.Status status);

    void deleteApplicationById(Integer id);
}
