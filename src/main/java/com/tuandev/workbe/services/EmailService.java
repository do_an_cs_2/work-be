package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.EmailApplyDto;
import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.Employer;

public interface EmailService {
    void sendEmailApply(EmailApplyDto applyDto);

    void sendEmailUpdateStatus(Application applyDto);

    void sendEmailUpdateStatus(Employer employer);
}
