package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.FavoriteJobRequest;
import com.tuandev.workbe.dto.FavoriteJobResponse;
import com.tuandev.workbe.entities.FavoriteJob;

import java.util.List;

public interface FavoriteJobService {
    List<FavoriteJobResponse> getAllFavoriteJob();

    FavoriteJobResponse getFavoriteById(Integer id);

    List<FavoriteJobResponse> getFavoriteJobByUser(Integer userId);

    FavoriteJobResponse createFavoriteJob(FavoriteJobRequest favoriteJobRequest);

    void deleteFavoriteJobById(Integer id);

    FavoriteJobResponse getFavoriteJob(Integer userId, Integer jobId);
}
