package com.tuandev.workbe.services;

import com.tuandev.workbe.entities.Field;

import java.util.List;
import java.util.Optional;

public interface FieldService {
    Optional<Field> getFieldById(Integer id);

    List<Field> getAllFields();

    void deleteFieldById(Integer id);
}
