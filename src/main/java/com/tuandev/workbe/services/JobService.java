package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.JobDto;
import com.tuandev.workbe.entities.Job;

import java.util.List;
import java.util.Optional;

public interface JobService {
    List<Job> getAllJobs();

    List<Job> getJobByCompany(Integer companyId);

    List<Job> getJobByName(String name);

    List<Job> getJobByField(Integer fieldId);

    Optional<Job> getJobById(Integer id);

    Job createJob(JobDto jobDto);

    void deleteJobById(Integer id);
}
