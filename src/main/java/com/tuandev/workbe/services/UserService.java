package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.UserDto;
import com.tuandev.workbe.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> getUserById(Integer id);

    Optional<User> getUserByEmail(String email);

    User creeateUser(UserDto userDto);

    List<User> getAllUsers();

    User updateUser(Integer id, User user);

    void deleteUser(Integer id);
}
