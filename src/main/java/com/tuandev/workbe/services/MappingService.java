package com.tuandev.workbe.services;

public interface MappingService {
    <T, U> U map(T source, Class<U> destinationType);
}
