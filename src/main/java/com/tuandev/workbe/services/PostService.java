package com.tuandev.workbe.services;

import com.tuandev.workbe.dto.PostRequest;
import com.tuandev.workbe.entities.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {

    List<Post> getAllPosts();

    List<Post> getPostByEmployer(Integer employerId);

    Post getPostById(Integer id);

    Post createPost(PostRequest postRequest);

    Post updatePost(Integer id, PostRequest postRequest);

    void deletePost(Integer id);
}
