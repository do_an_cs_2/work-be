package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.FavoriteJobRequest;
import com.tuandev.workbe.dto.FavoriteJobResponse;
import com.tuandev.workbe.entities.FavoriteJob;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import com.tuandev.workbe.repositories.FavoriteJobRepository;
import com.tuandev.workbe.services.FavoriteJobService;
import com.tuandev.workbe.services.JobService;
import com.tuandev.workbe.services.MappingService;
import com.tuandev.workbe.services.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FavoriteJobServiceImpl implements FavoriteJobService {
    private final FavoriteJobRepository favoriteJobRepository;
    private final MappingService mappingService;
    private final JobService jobService;
    private final UserService userService;

    @Override
    public List<FavoriteJobResponse> getAllFavoriteJob() {
        List<FavoriteJob> favoriteJobs = favoriteJobRepository.findAll();
        return favoriteJobs.stream().map(favoriteJob -> mappingService.map(favoriteJob, FavoriteJobResponse.class)).collect(Collectors.toList());
    }

    @Override
    public FavoriteJobResponse getFavoriteById(Integer id) {
        return favoriteJobRepository.findById(id).map(favoriteJob -> mappingService.map(favoriteJob, FavoriteJobResponse.class)).orElseThrow(() -> new NoSuchElementException("Favorite Job not found with Id = " + id));
    }

    @Override
    public List<FavoriteJobResponse> getFavoriteJobByUser(Integer userId) {
        User user = userService.getUserById(userId).get();
        List<FavoriteJob> favoriteJobs = favoriteJobRepository.findFavoriteJobByUser(user);
        return favoriteJobs.stream().map(favoriteJob -> mappingService.map(favoriteJob, FavoriteJobResponse.class)).collect(Collectors.toList());
    }

    @Override
    public FavoriteJobResponse createFavoriteJob(FavoriteJobRequest favoriteJobRequest) {
        User user = userService.getUserById(favoriteJobRequest.getUserId()).get();
        Job job = jobService.getJobById(favoriteJobRequest.getJobId()).get();
        FavoriteJob favoriteJob = FavoriteJob.builder()
                .user(user)
                .job(job)
                .build();
        favoriteJob = favoriteJobRepository.save(favoriteJob);
        return mappingService.map(favoriteJob, FavoriteJobResponse.class);
    }

    @Override
    public void deleteFavoriteJobById(Integer id) {
        if (favoriteJobRepository.findById(id).isPresent()) {
            favoriteJobRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException("Favorite Job Not Found with ID = " + id);
        }
    }

    @Override
    public FavoriteJobResponse getFavoriteJob(Integer userId, Integer jobId) {
        User user = userService.getUserById(userId).get();
        Job job = jobService.getJobById(jobId).get();
        return mappingService.map(favoriteJobRepository.findByUserAndJob(user, job), FavoriteJobResponse.class);
    }
}
