package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.CompanyDto;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.repositories.CompanyRepository;
import com.tuandev.workbe.services.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;

    @Override
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    @Override
    public Optional<Company> getCompanyById(Integer id) {
        return companyRepository.findById(id);
    }

    @Override
    public Company createCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public Company updateCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public void deleteCompanyById(Integer id) {
        companyRepository.deleteById(id);
    }
}
