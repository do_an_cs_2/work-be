package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.EmailApplyDto;
import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.Employer;
import com.tuandev.workbe.services.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;

    private String fromEmail = "WorldWork.com";

    @Override
    public void sendEmailApply(EmailApplyDto applyDto) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(fromEmail);
        simpleMailMessage.setTo(applyDto.getUser().getEmail());
        simpleMailMessage.setSubject("Application Confirmation for " + applyDto.getJob().getName());

        String emailContent = "Dear " + applyDto.getUser().getFullName() + ",\n\n" +
                "Thank you for your interest and for applying for the position of " +
                applyDto.getJob().getName() + " at our company.\n" +
                "We have received your application and will review it carefully.\n" +
                "The processing of applications may take some time, and we will notify you of the results as soon as possible.\n\n" +
                "While waiting, please explore more information about us and other career opportunities on our website.\n" +
                "If you have any questions or need additional information, feel free to contact us via email at " + fromEmail + " or by phone.\n\n" +
                "We appreciate your interest and look forward to the possibility of working together in the near future.\n\n" +
                "Best of luck!";

        simpleMailMessage.setText(emailContent);

        javaMailSender.send(simpleMailMessage);
    }

    @Override
    public void sendEmailUpdateStatus(Application applyDto) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setFrom(fromEmail);
        simpleMailMessage.setTo(applyDto.getUser().getEmail());
        simpleMailMessage.setSubject("Notification: Application Status Update");

        String emailContent = "Dear " + applyDto.getUser().getFullName() + ",\n\n" +
                "We would like to inform you that the status of your job application has been updated.\n" +
                "New status: " + applyDto.getStatus().name() + "\n\n" +
                "Thank you for your interest in our job opport  unity.\n" +
                "If you have any questions, please contact us at " + fromEmail + ".\n\n" +
                "Best regards,\n" + applyDto.getJob().getCompany().getName();

        simpleMailMessage.setText(emailContent);

        javaMailSender.send(simpleMailMessage);
    }

    @Override
    public void sendEmailUpdateStatus(Employer employer) {

    }
}
