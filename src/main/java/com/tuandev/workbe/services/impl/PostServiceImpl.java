package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.PostRequest;
import com.tuandev.workbe.entities.Employer;
import com.tuandev.workbe.entities.Post;
import com.tuandev.workbe.repositories.PostRepository;
import com.tuandev.workbe.services.EmployerService;
import com.tuandev.workbe.services.PostService;
import com.tuandev.workbe.services.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {
    private final EmployerService employerService;
    private final PostRepository postRepository;

    @Override
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> getPostByEmployer(Integer employerId) {
        Employer employer = employerService.getEmployerById(employerId);
        return postRepository.findByEmployer(employer);
    }

    @Override
    public Post getPostById(Integer id) {
        return postRepository.findById(id).orElseThrow(() -> new NoSuchElementException("NO CUSTOMER PRESENT WITH ID = " + id));
    }



    @Override
    public Post createPost(PostRequest postRequest) {
        Employer employer = employerService.getEmployerById(postRequest.getEmployerId());

        Post post = Post.builder()
                .title(postRequest.getTitle())
                .introduction(postRequest.getIntroduction())
                .content(postRequest.getContent())
                .employer(employer)
                .createdAt(new Date())
                .build();
        return postRepository.save(post);
    }

    @Override
    public Post updatePost(Integer id, PostRequest postRequest) {
        Post post = postRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Post not found with id: " + id));
        post.setTitle(postRequest.getTitle());
        post.setIntroduction(postRequest.getIntroduction());
        post.setContent(postRequest.getContent());
        return postRepository.save(post);
    }

    @Override
    public void deletePost(Integer id) {
        if (postRepository.existsById(id)) {
            postRepository.deleteById(id);
        } else {
            throw new NoSuchElementException("Post not found with id: " + id);
        }
    }
}
