package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.entities.Field;
import com.tuandev.workbe.repositories.FieldRepository;
import com.tuandev.workbe.services.FieldService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FieldServiceImpl implements FieldService {
    private final FieldRepository fieldRepository;

    @Override
    public Optional<Field> getFieldById(Integer id) {
        return fieldRepository.findById(id);
    }

    @Override
    public List<Field> getAllFields() {
        return fieldRepository.findAll();
    }

    @Override
    public void deleteFieldById(Integer id) {
        fieldRepository.deleteById(id);
    }
}
