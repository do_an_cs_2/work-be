package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.JobDto;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Field;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.repositories.JobRepository;
import com.tuandev.workbe.services.CompanyService;
import com.tuandev.workbe.services.FieldService;
import com.tuandev.workbe.services.JobService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JobServiceImpl implements JobService {
    private final JobRepository jobRepository;
    private final FieldService fieldService;
    private final CompanyService companyService;

    @Override
    public List<Job> getAllJobs() {
        return jobRepository.findAll();
    }

    @Override
    public List<Job> getJobByCompany(Integer companyId) {
        Optional<Company> company = companyService.getCompanyById(companyId);
        return company.map(jobRepository::findByCompany).orElse(null);
    }

    @Override
    public List<Job> getJobByName(String name) {
        return jobRepository.findByNameContains(name);
    }

    @Override
    public List<Job> getJobByField(Integer fieldId) {
        Optional<Field> fieldOptional = fieldService.getFieldById(fieldId);
        return fieldOptional.map(jobRepository::findByField).orElse(null);
    }

    @Override
    public Optional<Job> getJobById(Integer id) {
        return jobRepository.findById(id);
    }

    @Override
    public Job createJob(JobDto jobDto) {
        Job job = Job.builder()
                .name(jobDto.getName())
                .description(jobDto.getDescription())
                .offer(jobDto.getOffer())
                .requirement(jobDto.getRequirement())
                .location(jobDto.getLocation())
                .salary(jobDto.getSalary())
                .company(jobDto.getCompany())
                .field(jobDto.getField())
                .createdAt(new Date())
                .feature(false)
                .build();
        return jobRepository.save(job);
    }

    @Override
    public void deleteJobById(Integer id) {
        jobRepository.deleteById(id);
    }
}
