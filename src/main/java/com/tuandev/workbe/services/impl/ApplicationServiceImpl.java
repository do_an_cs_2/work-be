package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.ApplicationDto;
import com.tuandev.workbe.entities.Application;
import com.tuandev.workbe.entities.Job;
import com.tuandev.workbe.entities.User;
import com.tuandev.workbe.repositories.ApplicationRepository;
import com.tuandev.workbe.repositories.JobRepository;
import com.tuandev.workbe.services.ApplicationService;
import com.tuandev.workbe.services.JobService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {
    private final ApplicationRepository applicationRepository;
    private final JobRepository jobRepository;

    @Override
    public Optional<Application> getApplicationById(Integer id) {
        return applicationRepository.findById(id);
    }

    @Override
    public List<Application> getAllApplications() {
        return applicationRepository.findAll();
    }

    @Override
    public List<Application> getApplicationsByUser(User user) {
        return applicationRepository.findByUser(user);
    }

    @Override
    public List<Application> getApplicationByJob(Integer jobId) {
        Optional<Job> jobOptional = jobRepository.findById(jobId);
        return jobOptional.map(applicationRepository::findByJob).orElse(null);
    }

    public Application createApplication(Application application) {
        return applicationRepository.save(application);
    }

    @Override
    public Application updateApplicationStatus(Integer id, Application.Status status) {
        Optional<Application> application = applicationRepository.findById(id);
        if (application.isPresent()) {
            Application application1 = application.get();
            application1.setStatus(status);
            return applicationRepository.save(application1);
        }
        return null;
    }

    @Override
    public void deleteApplicationById(Integer id) {
        applicationRepository.deleteById(id);
    }

}
