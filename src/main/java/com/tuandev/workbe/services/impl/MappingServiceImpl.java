package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.services.MappingService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MappingServiceImpl implements MappingService {
    private final ModelMapper modelMapper;

    @Override
    public <T, U> U map(T source, Class<U> destinationType) {
        return modelMapper.map(source, destinationType);
    }
}
