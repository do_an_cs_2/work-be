package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.EmployerDto;
import com.tuandev.workbe.entities.Company;
import com.tuandev.workbe.entities.Employer;
import com.tuandev.workbe.repositories.EmployerRepository;
import com.tuandev.workbe.services.CompanyService;
import com.tuandev.workbe.services.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployerServiceImpl implements EmployerService {
    private final EmployerRepository employerRepository;
    private final CompanyService companyService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<Employer> getAllEmployers() {
        return employerRepository.findAll();
    }

    @Override
    public Employer getEmployerById(Integer id) {
        return employerRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Employer not found."));
    }

    @Override
    public Optional<Employer> getEmployerByEmail(String email) {
        return employerRepository.findByEmail(email);
    }

    @Override
    public Employer createEmployer(EmployerDto employerDto) {
        Optional<Company> company = companyService.getCompanyById(employerDto.getCompany().getCompanyId());
        if (company.isEmpty()) {
            return null;
        }
        Employer employer = Employer.builder()
                .name(employerDto.getName())
                .email(employerDto.getEmail())
                .password(passwordEncoder.encode(employerDto.getPassword()))
                .createdAt(new Date())
                .company(employerDto.getCompany())
                .build();
        return employerRepository.save(employer);
    }

    @Override
    public Employer updateEmployer(Integer id, EmployerDto employerDto) {
        return null;
    }

    @Override
    public void deleteEmployer(Integer id) {

    }
}
