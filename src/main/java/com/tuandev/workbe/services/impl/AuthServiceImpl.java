package com.tuandev.workbe.services.impl;

import com.tuandev.workbe.dto.AuthDto;
import com.tuandev.workbe.dto.ResponseJwt;
import com.tuandev.workbe.entities.User;
import com.tuandev.workbe.repositories.UserRepository;
import com.tuandev.workbe.services.AuthService;
import com.tuandev.workbe.services.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public ResponseJwt login(AuthDto authDto) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authDto.getEmail(), authDto.getPassword()));
        User user = userRepository.findByEmail(authDto.getEmail()).orElseThrow(() -> new IllegalArgumentException("Invalid email"));
        return ResponseJwt.builder()
                .token(jwtService.generateToken(user))
                .refreshToken(jwtService.generateRefreshToken(user))
                .build();
    }

}
